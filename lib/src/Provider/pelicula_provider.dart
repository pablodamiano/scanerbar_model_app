import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:model_barcode_app/src/Model/actores_model.dart';
import 'package:model_barcode_app/src/Model/pelicula_movel.dart';

class PeliculasProvider {

  String _apikey = '8ed93b3b73646150b89042d1bcb029ab';
  String _url = 'api.themoviedb.org';
  String _language = 'es-ES';

  int _popularespage = 0;

  bool _cargando = false;

  List<Pelicula> _populares = new List();

  final _popularesStreamController = StreamController<List<Pelicula>>.broadcast();
 
  Function(List<Pelicula>)get popularesSink => _popularesStreamController.sink.add;

  Stream<List<Pelicula>> get popularesStream => _popularesStreamController.stream;

  void disposeStream(){

    _popularesStreamController?.close();
  }

  Future<List<Pelicula>> _procesarRespuesta(Uri url) async{
    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);
    //print(decodeData['results']); 
    final peliculas = new Peliculas.fromJsonList(decodeData['results']);
    //print(peliculas.items[1].title);
    
    return peliculas.items;


  }
  Future<List<Pelicula>> getEnCines() async {
      final url = Uri.https(_url, '3/movie/now_playing', {
        'api_key': _apikey,
        'language': _language,
      });

      return await _procesarRespuesta(url);



  }
  Future<List<Pelicula>> getPopulares() async {

    if(_cargando){ return[];   }
    _cargando = true;
    _popularespage++;
    print('Cargando Datas');
      final url = Uri.https(_url, '3/movie/popular', {
        'api_key': _apikey,
        'language': _language,
        'page' :_popularespage.toString(),
      });

    final resp  = await _procesarRespuesta(url);

    _populares.addAll(resp);

    popularesSink(_populares);
     _cargando = false;
    return resp;
  }

  Future<List<Actor>> getCast(String peliID)async{

    final url = Uri.https(_url,'3/movie/$peliID/credits',{
      'api_key': _apikey,
     // '_language' :_language,
    });
    print(url);
    final resp = await http.get(url);
    final decodeData = json.decode(resp.body);

    final cast = new Cast.fromJsonList(decodeData['cast']);
    return cast.actores;
    
  }

  Future<List<Pelicula>> buscarPleicula(String query) async {
      final url = Uri.https(_url, '3/search/movie', {
        'api_key': _apikey,
        'language': _language,
        'query':query,
      });

      return await _procesarRespuesta(url);
  }  

}