import 'package:flutter/material.dart';
import 'package:model_barcode_app/src/Pages/initial_page.dart'; 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: '/',
       routes: {
         '/':(BuildContext context) =>BarcodePage(),
       } ,
    );
  }
}

class InitialPage {
}